FROM node
MAINTAINER Luis Izquierdo
WORKDIR /app
COPY . .
RUN npm install 
EXPOSE 3000
CMD nmp start