const express = require('express');

// RESTFULL => GET, POST, PUT, PATCH y DELETE
// Modelo = una estructura de datos que representa una entdad del mundo real
function list(req, res, next) {
    res.send('Lista de usuarios del sistema');
}

function index(req, res, next){
    res.send(`Usuario del sistema con un ID ${req.params.id}`);
}

function create(req, res, next){
    const name = req.body.name;
    const lastName = req.body.lastName;
    res.send(`Crear un nuevo usuario con nombre ${name} y apellido ${lastName}`);
}

function replace(req, res, next){
    res.send(`Remplazo un usuario con un ID ${req.params.id}`);
}

function edit(req, res, next){
    res.send(`Remplazo un usuario con un ID ${req.params.id}`);
}

function destroy(req, res, next){
    res.send(`Elimino un usuario con un ID ${req.params.id}`);
}

module.exports = {
    list, index, create, replace, edit, destroy
}